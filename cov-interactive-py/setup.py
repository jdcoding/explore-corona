from setuptools import setup

setup(
   name='cov_interactive_py',
   version='1',
   description='Python code used on the CovInteractive website',
   author='Johannes Döllinger',
   author_email='johannes.doellinger@gmail.com',
)