from cov_interactive_py.data_import.import_all_data import import_all_data, write_data

if __name__ == "__main__":
    data = import_all_data()
    write_data(data)

