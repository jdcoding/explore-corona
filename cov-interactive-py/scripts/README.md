# Data import

Imports and merges data from the [Robert Koch Institute](https://www.rki.de/DE/Home/homepage_node.html) ([source](https://www.arcgis.com/home/item.html?id=dd4580c810204019a7b8eb3e0b329dd6))  and the [Humanistic GIS Lab](https://hgis.uw.edu/) ([source](https://github.com/jakobzhao/virus)).

To import and merge the data run:

```bash
python import_all_data.py
```

which will automatically refresh [static/assets/whole_parsed_data.csv](../static/assets/whole_parsed_data.csv]).