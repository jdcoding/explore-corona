import re
import argparse


def correct_topojson(fname):
    """
    Corrects TopoJSON properties to match the name .
    @param fname: TopoJSON file.
    """

    do_enname = True
    changed = False

    with open(fname, 'r', encoding='utf8') as file:
        data = file.read().replace('\n', '')

        if do_enname:
            if 'enname' not in data:
                # Don't make changes twice!
                pattern = re.compile(r'"NAME_1":"(.*?)"')
                for m in re.finditer(pattern, data):
                    NAME_1 = m.groups()[0]
                    enname = NAME_1.lower()
                    data = data.replace('"NAME_1":"' + NAME_1 + '"', '"NAME_1":"' + NAME_1 + '","enname":"' + enname + '"')
                changed = True
            else:
                print('Not adjusting enname, as file already contains it.')

    if changed:
        with open(fname, 'w', encoding='utf8') as file:
            file.write(data)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()  # init ArgumentParser
    parser.add_argument('--file', type=str, default='../../static/assets/bundeslaender.json')
    args = parser.parse_args()

    correct_topojson(args.file)
