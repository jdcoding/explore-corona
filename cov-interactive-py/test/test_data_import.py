import pytest
from cov_interactive_py.data_import.HGIS_importer import HGISImporter
from cov_interactive_py.data_import.RKI_importer import RKIImporter


def test_hgis_importer():
    HGISImporter().import_data()


@pytest.mark.parametrize('landkreis', [True, False])
def test_rki_importer(landkreis):
    RKIImporter(split_landkreis=landkreis).import_data()
