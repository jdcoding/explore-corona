import pytest

from cov_interactive_py.epidemy_model.epidemy_data \
    import update_timeseries_plot_callback, load_last_values, update_history_plot_callback, paneldata_callback


@pytest.mark.parametrize('region', ['bayern', 'czech republic', 'united kingdom', 'congo', 'congo kinshasa'])
def test_prediction_bayern(region):
    prediction_horizon = 100
    update_timeseries_plot_callback(region, prediction_horizon)


def test_history_bayern():
    region_name = 'bayern'
    update_history_plot_callback(region_name)


def test_panel_data_bayern():
    paneldata_callback('bayern')


def test_load_last_values():
    load_last_values()

