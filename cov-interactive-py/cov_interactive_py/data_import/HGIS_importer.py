import os
import json

import pandas as pd
from cov_interactive_py.data_import.data_format import DATA_FORMAT, to_format_str


def _replace_names_according_to_json(data: pd.DataFrame):
    here = os.path.split(__file__)[0]
    with open(os.path.join(here, '../../..', 'static', 'country_names.json')) as f:
        translation_dict = json.load(f)

    translation_dict = dict([[v['hgis_name'], k] for k, v in
                             filter(lambda el: 'hgis_name' in el[1].keys(), translation_dict.items())])
    data.rename(columns=translation_dict, inplace=True)
    return data


class HGISImporter(object):
    # current virus csv from https://github.com/jakobzhao/virus
    _csv_url = "https://hgis.uw.edu/virus/assets/virus.csv"
    _date_unified_format = "2020-01-28"  # before this date the format does not allow to reliably resolve variables
    _duplicate_countries = ['us', 'canada']  # countries with both the global and the local numbers

    def import_data(self):
        data = pd.read_csv(self._csv_url)
        data = data[data['datetime'] >= self._date_unified_format]
        data = _replace_names_according_to_json(data)
        return self._generate_global_trend(data)

    def _generate_global_trend(self, data: pd.DataFrame):
        global_data_dict = {}
        for ix, key in enumerate(DATA_FORMAT):
            numeric = data.copy().drop(['datetime'], axis=1)
            for column in numeric.columns:

                def extract_value(x):
                    if isinstance(x, str):
                        x = x.split('-')
                        if len(x) != 4:
                            return float('nan')
                        else:
                            try:
                                return int(x[ix])
                            except:
                                return float('nan')
                    else:
                        return float('nan')

                numeric[column] = numeric[column].transform(func=extract_value)
            # Subtract US and Canada wide data as states are already counted
            global_data = numeric.sum(axis=1) - numeric[self._duplicate_countries].sum(axis=1)
            global_data_dict[key] = global_data

        global_data = pd.DataFrame(global_data_dict)
        data['Weltweit'] = global_data.apply(lambda x: to_format_str(**x), axis=1)
        return data
