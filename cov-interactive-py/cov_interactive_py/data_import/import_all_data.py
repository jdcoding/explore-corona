import datetime
import os

import pytz
import pandas as pd

from cov_interactive_py.data_import.HGIS_importer import HGISImporter
from cov_interactive_py.data_import.RKI_importer import RKIImporter



def data_paths():
    here = os.path.split(__file__)[0]
    data_path = os.path.join(here, "../../../static/assets/whole_parsed_data.csv")
    time_stamp_path = os.path.join(here, "../../../static/assets/timestamp.txt")  # access timestamp file
    return data_path, time_stamp_path


def write_data(updated_data):
    data_path, time_stamp_path = data_paths()
    pd.DataFrame(updated_data).to_csv(data_path, index=False)
    timestamp = get_time_stamp()
    with open(time_stamp_path, "w") as f:  # open file
        text = f.write("timestamp\n{}".format(timestamp))  # overwrite old timestamp
    print("Updated data successfully!")


def get_time_stamp():
    utc_now = pytz.utc.localize(datetime.datetime.utcnow())
    de_now = utc_now.astimezone(pytz.timezone("Europe/Berlin"))
    timestamp = datetime.datetime.strftime(de_now, "%Y-%m-%d %H:%M:%S.%f")  # format timestamp as string
    return timestamp


def import_all_data():
    rki_importer_bl = RKIImporter(False)
    rki_importer_lk = RKIImporter(True)

    print("Importing RKI data...")
    rki_bl_data = rki_importer_bl.import_data()
    rki_lk_data = rki_importer_lk.import_data()

    data = rki_bl_data.merge(rki_lk_data, on="datetime", how='outer')
    print("RKI data imported.")

    print("Importing HGIS data...")
    hgis_importer = HGISImporter()
    hgis_data = hgis_importer.import_data()
    data = data.merge(hgis_data, on="datetime", how='outer')
    print("HGIS data imported")

    data = data.sort_values('datetime')
    return data.to_dict()


