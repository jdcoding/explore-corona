from typing import Optional

DATA_FORMAT = ['confirmed', 'suspected', 'cured', 'dead']


def to_format_str(confirmed: Optional[int] = None, suspected: Optional[int] = None, cured: Optional[int] = None,
                  dead: Optional[int] = None) -> str:
    data_dict = locals()
    return '{}-{}-{}-{}'.format(*[_reformat(data_dict[df]) for df in DATA_FORMAT])


def _reformat(case: Optional[int]):
    return 'nan' if case is None else '{}'.format(int(case))


if __name__ == "__main__":
    print(to_format_str(confirmed=10, dead=20))
