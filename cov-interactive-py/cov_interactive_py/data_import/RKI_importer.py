import argparse
import datetime

import numpy as np
import pandas as pd
import requests
from cov_interactive_py.data_import.data_format import to_format_str


class RKIImporter(object):
    # RKI does not report cured cases
    # RKI does not report suspected cases
    # Data from Robert Koch institute
    # https://npgeo-corona-npgeo-de.hub.arcgis.com/datasets/dd4580c810204019a7b8eb3e0b329dd6_0/geoservice
    # https://www.arcgis.com/home/item.html?id=dd4580c810204019a7b8eb3e0b329dd6
    _data_url = "https://opendata.arcgis.com/datasets/dd4580c810204019a7b8eb3e0b329dd6_0.geojson"

    def __init__(self, split_landkreis: bool):
        self._split_landkreis = split_landkreis

    def _reformat_whole_dataframe(self, data: pd.DataFrame):
        data["data"] = data.apply(lambda x: to_format_str(confirmed=x['AnzahlFall'], dead=x['AnzahlTodesfall']), axis=1)
        if self._split_landkreis:
            data = data[["Meldedatum", "Landkreis", "data"]]
            data = pd.pivot_table(data, index=["Meldedatum"], columns=["Landkreis"],
                                  aggfunc=lambda x: "".join(str(v) for v in x))
        else:
            data = data[["Meldedatum", "Bundesland", "data"]]
            data = pd.pivot_table(data, index=["Meldedatum"], columns=["Bundesland"],
                                  aggfunc=lambda x: "".join(str(v) for v in x))
        data = data.droplevel(None, axis=1).reset_index()
        # Align with virus.csv use datetime header
        data.columns = ['datetime' if col == 'Meldedatum' else col.lower() for col in data.columns]
        return data

    def _reformat_features(self, data: pd.DataFrame):
        data = data[["AnzahlFall", "AnzahlTodesfall", "Landkreis", "Meldedatum"]] if self._split_landkreis else data[
            ["AnzahlFall", "AnzahlTodesfall", "Bundesland", "Meldedatum"]]  # only keep needed columns

        l = []
        for col in data.columns:  # go through all columns
            l.append(data[data[col] == "-nicht erhoben-"])  # append all rows which contain a "-nicht erhoben-"
        data = data.drop(pd.concat(l).index, axis=0).reset_index(drop=True) if len(
            l) > 0 else data  # drop all rows which contain a "-nicht erhoben-"
        data["Meldedatum"] = data["Meldedatum"].apply(
            lambda x: datetime.datetime.strftime(datetime.datetime.strptime(x, "%Y/%m/%d %H:%M:%S"), "%Y-%m-%d"))

        if self._split_landkreis:  # if landkreise shall be kept
            pivot_data = pd.pivot_table(data, index=["Meldedatum", "Landkreis"],
                                        aggfunc=np.sum).reset_index()  # make pivot with all "AnzahlFall/AnzahlTodesfall" per day
            return pd.pivot_table(pivot_data.set_index(["Landkreis", "Meldedatum"]), index="Landkreis",
                                  aggfunc=np.cumsum).reset_index().sort_values(by="Meldedatum").reset_index(
                drop=True)  # cumsum over the days
        else:
            # otherwise
            pivot_data = pd.pivot_table(data, index=["Meldedatum", "Bundesland"],
                                        aggfunc=np.sum).reset_index()  # make pivot with all "AnzahlFall/AnzahlTodesfall" per day
            return pd.pivot_table(pivot_data.set_index(["Bundesland", "Meldedatum"]), index="Bundesland",
                                  aggfunc=np.cumsum).reset_index().sort_values(by="Meldedatum").reset_index(
                drop=True)  # cumsum over the days

    def import_data(self):
        r = requests.get(self._data_url).json()
        raw_data = r["features"]  # get json with data, returns a list
        data = pd.json_normalize(raw_data)
        data.rename(columns = {
            'properties.AnzahlFall': 'AnzahlFall',
            'properties.AnzahlTodesfall': 'AnzahlTodesfall',
            'properties.Landkreis': 'Landkreis',
            'properties.Bundesland': 'Bundesland',
            'properties.Meldedatum': 'Meldedatum',
        }, inplace = True)
        # frames = []
        # for d in raw_data:  # go through list
        #     frames.append(
        #         pd.DataFrame.from_dict(d["properties"], orient="index").T)  # add transformed DataFrame to new list
        # data = pd.concat(frames).reset_index(drop=True)  # concat list of DataFrames and reset_index
        data = self._reformat_features(data)

        data = self._reformat_whole_dataframe(data)  # reformat in new format that matches "virus.csv"
        return data


if __name__ == "__main__":
    parser = argparse.ArgumentParser()  # init ArgumentParser
    parser.add_argument('--landkreis', type=bool, default=False,
                        help='auf Landkreis- (=True) oder Bundesland (=False=default)-Ebene?')  # add argument
    args = parser.parse_args()

    di = RKIImporter(args.landkreis)
    data = di.import_data()
