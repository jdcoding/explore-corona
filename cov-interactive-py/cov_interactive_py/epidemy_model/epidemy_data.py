import os
import math
import warnings
import json
from datetime import datetime
from itertools import dropwhile
from dataclasses import dataclass
from functools import lru_cache

import numpy as np
import pandas as pd

from cov_interactive_py.epidemy_model.case_prediction \
    import set_beta_and_gamma, convert_displayed_to_SIR, compute_sir_value_table, convert_sir_to_displayed

here = os.path.split(__file__)[0]
POPULATION_DATA_PATH = os.path.join(here, '../../../static/assets/world_population.csv')
DATE_FORMAT = '%Y-%m-%d'


@lru_cache(maxsize=256)
def virus_data():
    VIRUS_DATA_PATH = os.path.join(here, '../../../static/assets/whole_parsed_data.csv')
    return pd.read_csv(VIRUS_DATA_PATH)


def paneldata_callback(region_name: str):
    full_data = virus_data()
    region_data = list(map(parse_virus_csv_cell, full_data[region_name]))
    newest_ix = max([i for i, x in enumerate(region_data) if x[0]])

    panel_values = list(map(format_panel_data, [region_data[newest_ix][1][i] for i in range(4)]))
    panel_diffs = list(map(format_panel_data,
                      [region_data[newest_ix][1][i] - region_data[newest_ix-1][1][i] for i in range(4)]))

    return {
        'dc': panel_values[0]
        , 'di': panel_values[1]
        , 'dr': panel_values[2]
        , 'dd': panel_values[3]
        , 'dc_diff': panel_diffs[0]
        , 'di_diff': panel_diffs[1]
        , 'dr_diff': panel_diffs[2]
        , 'dd_diff': panel_diffs[3]
    }


def format_panel_data(num: np.float):
    if not np.isnan(num):
        return int(num)
    else:
        return str(num)


def update_history_plot_callback(region_name: str):
    full_data = virus_data()
    date_time = list(map(parse_datetime, full_data['datetime']))
    region_data = list(map(parse_virus_csv_cell, full_data[region_name]))
    dc, di, dr, dd = map(lambda i: [str(row[1][i]) if row[0] else 'nan' for row in region_data], range(4))

    return [
        ['t'] + [datetime.fromordinal(date).strftime(DATE_FORMAT) for date in date_time],
        ['Infizierte gesamt'] + dc,
        ['Aktuell Infektiöse'] + di,
        ['Genesen'] + dr,
        ['Verstorben'] + dd,
    ]


def currentdata_callback():
    '''
    Report the last known value of every state by assuming monotonic increase of total confirmed, total recovered, and
    total deaths.
    '''
    data_splitted, data_current = load_last_values(initial_discard=no_cases, gapless=True)
    return data_current.to_csv(index=False)


def load_last_values(time_column_label: str = 'datetime', initial_discard=None, gapless: bool=False,
                     lookback: int=5):
    # Read full CSV file
    full_data = virus_data()
    # Interpret data backwards
    ret = {}
    populations = load_populations()
    for region_name in [x for x in full_data.keys() if x != 'datetime']:
        data_dict = data_point_iterable_to_dict_of_arrays(parse_virus_data_points(full_data,
            region_name, initial_discard=no_cases, gapless=True))

        data_dict = correct_data(data_dict, DataCorrectionParams(),
                                 unreported_cases_correction=True, fill_nans_di_dr=True)
        if len(data_dict['di']) and region_name in populations.keys():
            ret[region_name] = [data_dict['di'][-1] / populations[region_name] * 100000]
        else:
            ret[region_name] = [-1]

    return None, pd.DataFrame.from_dict(ret)


def update_timeseries_plot_callback(region_name: str, prediction_horizon: int = 100, beta=None, gamma=None):
    # Load data
    full_data = virus_data()
    data_dict = data_point_iterable_to_dict_of_arrays(parse_virus_data_points(full_data, region_name,
                                                                                    initial_discard=no_cases,
                                                                                    gapless=True))
    # Fixed defaults
    params = DataCorrectionParams()
    # Adjust or clean data
    data_dict = correct_data(data_dict, params,
                             monotonicity_correction=True, unreported_cases_correction=True, fill_nans_di_dr=True)
    # Estimate parameters
    N = load_populations()[region_name]
    beta, gamma = set_beta_and_gamma(beta, gamma, data_dict, N)
    gamma = max(0.003, gamma)  # Lower bound on gamma

    # Predict
    t, dc, di, dr, dd = data_dict.values()
    s0, i0, r0, d0 = convert_displayed_to_SIR(N, dc[-1], di[-1], dr[-1], dd[-1])
    t_pred = np.arange(0, prediction_horizon + 1) + t[-1]
    ms_pred, mi_pred, mr_pred, md_pred = compute_sir_value_table(t_pred, beta=beta, gamma=gamma, cfr=params.cfr, S0=s0,
                                                                 I0=i0, R0=r0, D0=d0).T
    dc_pred, di_pred, dr_pred, dd_pred = convert_sir_to_displayed(N, ms_pred, mi_pred, mr_pred, md_pred)

    return [
        ['t'] + [datetime.fromordinal(date).strftime(DATE_FORMAT) for date in np.hstack([t, t_pred[1:]]).tolist()],
        ['Infizierte gesamt'] + (dc).tolist() + dc_pred.tolist()[1:],
        ['Aktuell Infektiöse'] + (di).tolist() + di_pred.tolist()[1:],
        ['Genesen'] + (dr).tolist() + dr_pred.tolist()[1:],
        ['Verstorben'] + dd.tolist() + dd_pred.tolist()[1:],
        ]


@dataclass
class DataCorrectionParams():
    '''Default parameters based on public information.'''

    # Infection time
    # =========================
    # Average days with symptoms: 9 in 95,5% of cases
    # Average days from first symptoms until hospitality: 4 in 4,5% of cases
    # Average days in hospitals: 14 in 75,0% of cases
    # Average days after intensive hospitality after coming to hospital: 1 in 25,0% of cases
    # Average days of intensive hospitality: 10, then 50% recovered and 50% dead
    # https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Modellierung_Deutschland.pdf?__blob=publicationFile
    # https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Steckbrief.html#doc13776792bodyText9
    #
    # Lower bound on case duration after first symptoms: 8 days
    # 5. https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Steckbrief.html#doc13776792bodyText9
    # Upper bound on case duration after first symptoms: 8 days
    # 5. https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Modellierung_Deutschland.pdf?__blob=publicationFile

    # Average case duration:
    inf_time_avg: int = int(math.ceil(0.955 * 9 + 0.045 * (4 + (0.75 * 14 + 0.25 * (1 + 10)))))
    # Lower bound on case duration
    inf_time_min: int = 8
    inf_time_max: int = 4 + 14

    # Letality
    # ========
    # Estimate by RKI: 0,56%
    # https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Modellierung_Deutschland.pdf?__blob=publicationFile
    cfr: float = 0.0056

    # Unreported cases
    # ================
    # Estimate from china: 11 to 20 times higher
    # https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Steckbrief.html#doc13776792bodyText9
    # Estimate for germany: 7 to 10 times higher according to Laschet
    # https://www.deutschlandfunk.de/covid-19-wie-hoch-die-dunkelziffer-bei-den-coronavirus.1939.de.html?drn:news_id=1115891
    # Estimate for china: 7 times higher (86.6% undocumented cases)
    # https://science.sciencemag.org/content/early/2020/03/24/science.abb3221.full
    # German number should be smaller compared to chinese one, as make more tests.
    # https://www.deutschlandfunk.de/covid-19-wie-hoch-die-dunkelziffer-bei-den-coronavirus.1939.de.html?drn:news_id=1116353
    factor_unreported: float = 7.0





def parse_virus_data_points(full_data, region_name: str, time_column_label: str = 'datetime',
                                  initial_discard=None,
                                  gapless: bool=False):
    # Select subframe of interest
    region_data = full_data[[time_column_label, region_name]]
    # Parse datetime and virus data cells (result format [(dt, (accepted, pv))])
    parsed_data = list(map(lambda x: (parse_datetime(x[1]), parse_virus_csv_cell(x[2])), region_data.itertuples()))
    # Filter out rejected rows and discard acceptance flag (result format [(dt, pv)])
    filtered_data = list(map(lambda x: (x[0], x[1][1]), filter(lambda x: x[1][0], parsed_data)))
    # If an initial discard criterion is defined, drop further elements
    if initial_discard:
        filtered_data = list(dropwhile(initial_discard, filtered_data))
    # Return iterable of pairs of datetime and parsed cell contents
    if gapless:
        return list(gapless_data(filtered_data))
    else:
        return filtered_data


def parse_datetime(dt: str):
    return datetime.strptime(dt, DATE_FORMAT).toordinal()


def parse_virus_csv_cell(content: str):
    # Return (True, (dc, di, dr, dd)) for accepted cells
    # and (False, None) for rejected cells
    if content == 'nan':
        return False, None
    else:
        try:
            numbers = content.split(sep='-')
        except AttributeError:
            return False, None
        except:
            return False, None
        else:
            try:
                while len(numbers) < 4:
                    numbers.append('0')
                numbers = np.array(list(map(lambda i: numbers[i] if numbers[i] != 'nan' else np.nan,
                                            [0, 2, 3]))).astype(float)
                di = numbers[0] - numbers[1] - numbers[2]
                return (True, np.array([numbers[0], di] + numbers[1:].tolist()))
            except:
                return False, None


def no_cases(ty):
    return ty[1][0] == 0


def data_point_iterable_to_dict_of_arrays(points):
    t_array, dc_array, di_array, dr_array, dd_array = data_point_iterable_to_arrays(points)
    return {'t': t_array, 'dc': dc_array, 'di': di_array, 'dr': dr_array, 'dd': dd_array}


def data_point_iterable_to_arrays(points):
    if points:
        ts, vs = zip(*points)
    else:
        return (np.array([]),)*5
    t_array = np.array(ts)
    dc_array, di_array, dr_array, dd_array = map(np.array, zip(*vs))
    return t_array, dc_array, di_array, dr_array, dd_array


def correct_data(data_dict, params: DataCorrectionParams,
                 monotonicity_correction=True, unreported_cases_correction=False, fill_nans_di_dr=False):
    '''
    Reported data is often incorrect as the number of recovered cases is not measured appropriately. However, the
    total number of infected cases and the total number of deaths is known very accurately. Hence, this function
    applies data correction based on information from RKI about typical course of the disease.
    https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Modellierung_Deutschland.pdf?__blob=publicationFile
    https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Steckbrief.html#doc13776792bodyText9

    For correction, the approach assumes that every case of Corona lasts exactly avg_infection_time days. Afterwards, a
    case is either recovered or dead. If validity_check is set to false, the full dataset is adopted to match this
    assumption. If validity_check is set to true, samples are only corrected, if data does not satisfy the RKI
    assumption that every case lasts between 12 and 22 days.

    Correction approaches:
    - Total numbers (confirmed, recovered, dead) need to be monotonically increasing.
    - A case lasts between 12 and 22 days and afterwards is either recovered or dead.
    - If recovereds or infected not known, an average duration of infection of 12 days is assumed.

    Convert displayed or measured values to ones from SIR model.
    @param data_dict: Data dictionairy with keys being groups (dc, di, cr, dd) and values numpy arrays.
    @param monotonicity_correction: Turn on/off correction for monotonicity (total confirmed, recovered and dead)
    @param unreported_cases_correction: Turn on/off correction of unreported cases(total confirmed, infected, recovered)
    @param fill_nans_di_dr: Turn on/off inserting estimates into nans for current suspected, and total recovered.
    @return: Corrected data_dict
    '''

    # Correct for monotonicity of total confirmed, total recovered and total dead
    if monotonicity_correction:
        data_dict = correct_monotonicity(data_dict)

    # Correct for unreported cases in total confirmed, total recovered and total rec
    if unreported_cases_correction:
        data_dict = correct_unreported_cases(data_dict, params.factor_unreported)

    # Correct
    if fill_nans_di_dr:
        correct_nans_di_dr(data_dict, params.inf_time_avg)

    return data_dict


def correct_monotonicity(data_dict):
    # Total confirmed
    dc_monotone = np.fmax.accumulate(data_dict['dc'])
    if np.any(dc_monotone != data_dict['dc']):
        warnings.warn("Total confirmed not monotonically increasing. --> Corrected!")
    data_dict['dc'] = dc_monotone
    # Total recovered
    dr_monotone = np.fmax.accumulate(data_dict['dr'])
    if np.any(dr_monotone != data_dict['dr']):
        warnings.warn("Total recovered not monotonically increasing. --> Corrected!")
    data_dict['dr'] = dr_monotone
    # Total deaths
    dd_monotone = np.fmax.accumulate(data_dict['dd'])
    if np.any(dd_monotone != data_dict['dd']):
        warnings.warn("Total deaths not monotonically increasing. --> Corrected!")
    data_dict['dd'] = dd_monotone
    return data_dict


def correct_unreported_cases(data_dict, factor_unreported):
    # Total confirmed
    data_dict['dc'] = factor_unreported * data_dict['dc']
    # Current suspected
    data_dict['di'] = factor_unreported * data_dict['di']
    # Total recovered
    data_dict['dr'] = factor_unreported * data_dict['dr']
    return data_dict


def correct_nans_di_dr(data_dict, inf_time_avg):
    for ix in range(len(data_dict['t'])):
        # Calculate current suspected cases
        if np.isnan(data_dict['di'][ix]):
            if ix >= inf_time_avg:
                # Compute current suspected by integrating over additional cases in last days (avg_infection_time)
                C_tt = data_dict['dc'][ix - inf_time_avg:ix + 1]
                dC_t = C_tt[1:] - C_tt[:-1]
                I_t = np.sum(dC_t)
                data_dict['di'][ix] = max(0., I_t)
            else:
                data_dict['di'][ix] = 0.
        # Compute total recovered cases
        if np.isnan(data_dict['dr'][ix]):
            if ix >= inf_time_avg:
                # Compute current recovered cases by assuming all infected people are infected for exactly
                # avg_infection_time days. Afterwards, they are either dead(measured) or recovered.
                data_dict['dr'][ix] = max(0., data_dict['dc'][ix] - data_dict['di'][ix] - data_dict['dd'][ix])
            else:
                data_dict['dr'][ix] = 0.


def load_populations():
    population_data = pd.read_csv(POPULATION_DATA_PATH)
    ret = {}
    for k in population_data.keys():
        if k != 'datetime':
            ret[k] = int(population_data[k][0])
    return ret



class gapless_data(object):
    __slots__ = ['__it', '__curr_t', '__curr_dat', '__next_t', '__next_dat']

    def __init__(self, iterable):
        self.__it = iter(iterable)
        self.__curr_t = None
        self.__curr_dat = None
        self.__next_t = None
        self.__next_dat = None

    def __iter__(self):
        return self

    def __next__(self):
        if self.__curr_t is None:
            self.__curr_t, self.__curr_dat = next(self.__it)
            try:
                self.__next_t, self.__next_dat = next(self.__it)
            except StopIteration:
                self.__next_t = None
        else:
            if self.__next_t is None:
                raise StopIteration()
            self.__curr_t += 1
            if self.__curr_t < self.__next_t:
                pass
            else:
                self.__curr_dat = self.__next_dat
                try:
                    self.__next_t, self.__next_dat = next(self.__it)
                except StopIteration:
                    self.__next_t = None
        return self.__curr_t, self.__curr_dat


