"""
prediction of future case numbers based on the SIR-model https://de.wikipedia.org/wiki/SIR-Modell
"""

from functools import partial

import numpy as np
from scipy import optimize
from scipy import integrate

from typing import Union, Tuple


# --- ABSTRACT MODELLING ---

__cache_solution_for = {}
__cached_solution = {}
def numeric_model_funcs(model_ode, t_span, model_params, init_vals):
    """
    Return a function of time, which is a solution of the given initial value problem. Uses a simple cache for the last
    solved ivp per model.
    @param model_ode: The rhs of a system of ODEs.
    @param t_span: The time span, over which the system shall be integrated. Defines the times on which the return value
     is defined.
    @param model_params: A sequence of values for the parameters of the given model.
    @param init_vals: A sequence of initial values for the ivp to be solved.
    @return: A function of time, whose values are containers bundling the values of all unknowns of the ivp at the given
     time.
    """
    cache_marker = (t_span, model_params, init_vals)
    if model_ode in __cache_solution_for and __cache_solution_for[model_ode] == cache_marker:
        return __cached_solution[model_ode]
    else:
        ivp_solution = integrate.solve_ivp(partial(model_ode, model_params=model_params),
                                           t_span,
                                           init_vals,
                                           dense_output=True).sol
        __cache_solution_for[model_ode] = cache_marker
        __cached_solution[model_ode] = ivp_solution
        return ivp_solution


def fit_model_to_data(model_ode, n_model_params, ts, ys, i=0, t_span=None, fit_param_guess=None, bounds=None):
    """
    Fit the given ODE model to data, determining both the optimal model parameters and initial conditions.
    @param model_ode: A function representing the rhs of a system of ODEs.
    @param n_model_params: Number of parameters of the given model. Serves to disentangle parameters and initial conditions.
    @param ts: A sequence of data time values.
    @param ys: A sequence of data function values.
    @param i: Selects the function of the ODE model to be fitted to data. By default, selects the first function.
    @param t_span: Time span, over which the ODE model shall be integrated. By default, the minimum range covering all ts is chosen.
    @param bounds: Pair of sequences defining lower/upper bounds for the fit parameters. Defaults to no bounds.
    @param fit_param_guess: A sequence of guesses for both model parameters and initial values. Used as the starting point of the fit.
    @return: Sequence of fit results for model parameters, sequence of fit results for initial values
    """
    if t_span is None:
        t_span = (min(ts), max(ts))
    popt, _ = optimize.curve_fit(lambda t, *args: numeric_model_funcs(model_ode, t_span, args[:n_model_params], args[n_model_params:])(t)[i],
                                 ts, ys,
                                 p0=fit_param_guess,
                                 bounds=bounds)
    return popt[:n_model_params], popt[n_model_params:]


def compute_model_value_table(model_ode, model_params, init_vals, ts, t_span=None, i=None):
    """
    For the given ODE model and its parameters and initial values, compute a table of values.
    @param model_ode: A function representing the rhs of a system of ODEs.
    @param model_params: A sequence of values for the parameters of the given model, to be used in the computation.
    @param init_vals: A sequence of initial values for the model ivp, to be used in the computation.
    @param ts: A sequence of time values at which the model function shall be evaluated.
    @param t_span: The time span, for which the model ivp shall be solved. Defaults to the minimum range containing all ts.
    @param i: Selects one of the model functions to be used for the value table. Defaults to all functions.
    @return: Sequence of pairs with time value and model function value(s)
    """
    if t_span is None:
        t_span = (min(ts), max(ts))
    model_funcs = numeric_model_funcs(model_ode, t_span, model_params, init_vals)
    if i is None:
        return np.array([model_funcs(t) for t in ts])
    else:
        return np.array([model_funcs(t)[i] for t in ts])


# --- SIR SPECIFIC ---

model_sir_n_params = 3
model_sir_n_eq = 4
def model_sir_ode(_t, y, model_params):
    """
    Represents the rhs of the system of ODEs of the SIR model.
    """
    S, I, R, D = y
    N = S + I + R + D
    beta, gamma, cfr = model_params

    dSdt = -beta * S * I / N
    dIdt = beta * S * I / N - gamma * I
    dRdt = gamma * I * (1. - cfr)
    dDdt = gamma * I * cfr

    return dSdt, dIdt, dRdt, dDdt


def fit_sir_to_infection_data(ts, ys, t_span=None, bounds=None, param_guess=None):
    """
    Fit the SIR model to infection data I(t).
    @param ts: A sequence of time values.
    @param ys: A sequence of infection data values.
    @param t_span: Time span, over which the ODE model shall be integrated. By default, the minimum range covering all ts is chosen.
    @param bounds: Pair of sequences defining lower/upper bounds for the fit parameters. Defaults to no bounds.
    @param param_guess: A sequence of guesses for beta, gamma, S0, I0 and R0. Used as the starting point of the fit.
    @return: (beta, gamma), (S0, I0, R0)
    """
    return fit_model_to_data(model_sir_ode, model_sir_n_params,
                             ts, ys,
                             i=1,
                             t_span=t_span, bounds=bounds, fit_param_guess=param_guess)


def compute_sir_value_table(ts, beta, gamma, cfr, S0, I0, R0, D0, i=None):
    return compute_model_value_table(model_sir_ode, (beta, gamma, cfr), (S0, I0, R0, D0), ts, i=i)


def convert_displayed_to_SIR(population_size: Union[int, float], confirmed: Union[int, float],
                             infected_data: Union[int, float], recovered_data: Union[int, float],
                             dead: Union[int, float])\
        -> Tuple[Union[int, float], Union[int, float], Union[int, float], float]:
    '''
    Convert displayed or measured values to ones from SIR model.
    @param population_size: Population size.
    @param confirmed: Total number of cumulated, confirmed cases.
    @param infected_data: Current number of suspected / infected cases.
    @param recovered_data: Total number of cumulated, recovered cases.
    @param dead: Total number of dead cases.
    @return: Tuple of (s0 = #susceptible, i0 = #infectious, r0 = #recovered, d0 = #deaths)
    '''
    #recovered_estimated = dead * (1.-cfr) / cfr
    #estimation_correction_factor = recovered_estimated / recovered_data
    #cumulative_estimated, infected_estimated = estimation_correction_factor * np.array([confirmed, infected_data])
    #s = population_size - cumulative_estimated
    #r = dead + recovered_estimated

    s = population_size - confirmed
    i = infected_data
    r = recovered_data
    d = dead
    return s, i, r, d

def convert_sir_to_displayed(population_size: Union[int, float], suspectible: Union[int, float],
                             infectious: Union[int, float], recovered: Union[int, float],
                             dead: Union[int, float])\
        -> Tuple[Union[int, float], Union[int, float], Union[int, float], Union[int, float]]:
    '''
    Convert displayed or measured values to ones from SIR model.
    @param population_size: Population size.
    @param suspectible: Total number of suspectible people.
    @param infectious: Current number of infectious cases.
    @param recovered: Total number recovered cases (dead or really recovered).
    @param dead: Total number recovered cases (dead or really recovered).
    @return: Tuple of (#TotalConfirmedCases, #InfectiousCases, #RecoveredCases, #DeadCases)
    '''
    cumulative_confirmed = population_size - suspectible
    return cumulative_confirmed, infectious, recovered, dead


def compute_beta_from_sir(N, dmsdt, ms, mi):
    return -(N/mi)*(dmsdt/ms)


def compute_beta_from_displayed(N, ddcdt, dc, di):
    return np.float(1.)/(np.float(1.)-dc/N)*(ddcdt/di)


def compute_gamma_from_sir(dmrdt, mi):
    return dmrdt/mi


def compute_gamma_from_displayed(dddpdrdt, di):
    return dddpdrdt/di



def set_beta_and_gamma(beta, gamma, data_dict, N):
    if beta is None or gamma is None:
        beta2, gamma2 = estimate_beta_and_gamma(data_dict, N, 5)
        if beta is None:
            beta = beta2
        if gamma is None:
            gamma = gamma2
    return beta, gamma


def estimate_beta_and_gamma(data_dict, N, lookback: int = 5, spare_last_n_days: int=1):
    dt, dc, di, dr, dd = data_dict.values()

    beta = compute_beta_from_displayed(N, np.diff(dc)/np.diff(dt), dc[:-1], di[:-1])
    gamma = compute_gamma_from_displayed(np.diff(dd+dr)/np.diff(dt), di[:-1])

    # This estimation divides by the local derivative of infected people, which can be zero.
    # Hence, values can be inf or nan. However, an estimate of inf is not reasonable. Hence, we leave out infs for
    # estimation.
    beta[np.isinf(beta)] = np.nan
    gamma[np.isinf(gamma)] = np.nan

    # Estimate average values without considering nan values
    return np.nanmean(beta[-lookback-spare_last_n_days:-spare_last_n_days]),\
           np.nanmean(gamma[-lookback-spare_last_n_days:-spare_last_n_days])



