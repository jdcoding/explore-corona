"""
A simple flask server
"""
import atexit
from time import sleep
from os import environ as env


from flask import (Flask, jsonify)
from flask_cors import CORS
from rq import Queue

from apscheduler.schedulers.background import BackgroundScheduler
from worker import conn
from cov_interactive_py.epidemy_model.epidemy_data import update_timeseries_plot_callback, currentdata_callback, \
    update_history_plot_callback, paneldata_callback
from cov_interactive_py.data_import.import_all_data import import_all_data, write_data


app = Flask(__name__, static_url_path='')

# Check if environment variable is set, otherwise set it to 'config'
if 'APP_SETTINGS' in env:
    app.config.from_object(env['APP_SETTINGS'])
else:
    app.config.from_object('config')
CORS(app)


q = Queue(connection=conn)


def enqueue_download_data():
    job = q.enqueue_call(import_all_data)
    while job.result is None:
        sleep(10)
    new_data = job.result
    write_data(new_data)


# create schedule for printing time
scheduler = BackgroundScheduler()
scheduler.add_job(enqueue_download_data, 'interval', minutes=30)
scheduler.start()

# Shut down the scheduler when exiting the app
atexit.register(lambda: scheduler.shutdown())


@app.route('/')
def index():
    return app.send_static_file('index.html')


@app.route('/assets/currentdata.csv')
def currentdata():
    data = currentdata_callback()
    return data

@app.route('/paneldata/<country>')
def paneldata(country):
    data = paneldata_callback(country)
    return data

@app.route('/history/<country>')
def history(country):
    data = update_history_plot_callback(country)
    return jsonify({"status": "success", "country": country, "data": data})

@app.route('/predictions/<country>')
def predictions(country):
    data = update_timeseries_plot_callback(country)
    return jsonify({"status": "success", "country": country, "data": data})


if __name__ == '__main__':
    app.run()