



///////////////////////////////accessary//////////////////////////////////

$(window).ready(function() {
  $('.loader').fadeOut("slow");
});


var d3locale = d3.formatDefaultLocale({
    "thousands": ",",
    "grouping": [3],
});

L.TopoJSON = L.GeoJSON.extend({
  addData: function(jsonData) {
    if (jsonData.type === 'Topology') {
      for (key in jsonData.objects) {
        geojson = topojson.feature(jsonData, jsonData.objects[key]);
        L.GeoJSON.prototype.addData.call(this, geojson.features.filter(d=>d.properties.enname != "germany")); // exclude germany from top15 topojson
      }
    } else {
      L.GeoJSON.prototype.addData.call(this, jsonData);
    }
  }
});
// Copyright (c) 2013 Ryan Clark

// var createLabelIcon = function(labelClass, labelText) {
//   return L.divIcon({
//     className: labelClass,
//     html: labelText
//   })
// }
//


var worldBounds = L.latLngBounds(
  L.latLng(55, 6), //Northwest
  L.latLng(47, 15) //Southeast
);


//totalConfirmed
function tc(country) {
  if (country == undefined) {
    country = 0
  } else {
    country = +country.split("-")[0];
  }
  return country;
}

//totalRecovered
function tr(country) {
  if (country.split("-")[2] == undefined) {
    country = 0
  } else {
    country = +country.split("-")[2];
  }
  return country;
}

//totalDeath
function td(country) {
  if (country.split("-")[3] == undefined) {
    country = 0
  } else {
    country = +country.split("-")[3];
  }
  return country;
}

//////////////////////////////////////////////////////////////////////////





var mymap = L.map('map', {
  center: [50, 11],
  zoomControl: false,
  zoom: 8,
  maxZoom: 10,
  minZoom: 2,
  worldCopyJump: true
}).fitBounds(worldBounds);
// .fitWorld()
new L.Control.Zoom({
  position: 'topright'
}).addTo(mymap);



var osm = L.tileLayer('https://{s}.tile.openstreetmap.de/{z}/{x}/{y}.png').addTo(mymap);
var satellite =
    L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}');

var baseLayers = {
  'Open Street Maps': osm,
  'Satellit': satellite
}



var historyChart, predictionChart;

var colors = chroma.scale('YlOrRd').mode('lch').colors(6);
for (i = 0; i < 6; i++) {
  $('head').append($("<style> .region-color-" + (i + 1).toString() + " { color: " + colors[i] + "; font-size: 15px; text-shadow: 0 0 3px #ffffff;} </style>"));
  $('head').append($("<style> .legend-color-" + (i + 1).toString() + " { background: " + colors[i] + "; font-size: 15px; text-shadow: 0 0 3px #ffffff;} </style>"));
}



Promise.all([
  d3.csv('assets/whole_parsed_data.csv'), //datasets[0]
  d3.json("assets/all-topo-15.json"), //datasets[1]
  d3.csv('assets/communities.csv'), //datasets[2]
  d3.csv('assets/timestamp.txt'), //datasets[3]
  d3.csv('assets/cases.csv'), //datasets[4]
  d3.csv('assets/united-states.txt'), //datasets[5]
  d3.csv('assets/canada-city.txt'), //datasets[6]
  d3.csv('assets/old-name.csv'), //datasets[7]
  d3.csv('assets/cases-canada.csv'), //datasets[8]
  d3.csv('assets/germany.txt'), //datasets[9]
  d3.json('assets/bundeslaender.json'), //datasets[10] // bundeslaender landkreise_simplify200
  d3.csv('assets/parsed_api_data_bl.csv'), //datasets[11] parsed_api_data_lk
  d3.csv('assets/currentdata.csv'), //datasets[12] Data of last timestep
  d3.json('country_names.json') //datasets[13]
]).then(function(datasets) {

  $("#date").text(datasets[3][0].timestamp.split(".")[0]);

  var latest = datasets[0][datasets[0].length - 1];

  var top = {},
  ustop = {},
  cntop = {},
  detop = {};
  top["china"] = 0;

  Object.keys(latest).forEach(function(d) {
    value = tc(datasets[0][datasets[0].length - 1][d]) - tr(datasets[0][datasets[0].length - 1][d]) - td
   (datasets[0][datasets[0].length - 1][d]);
    // check if value in latest can be found in the regional text file
    // if yes, add them to the respective object variable

    if (datasets[7]["columns"].includes(d)) {
      // China
      top["china"] += +value;
      cntop[d] = value;
    } else if (datasets[9]["columns"].includes(d)) {
      // Germany
      detop[d] = value;
    } else if (datasets[5]["columns"].includes(d)) {
      // US
      ustop[d] = value;
    }
      else if (d != "datetime") {
      top[d] = value;
    }
  })



  function sortJsObject(obj) {
    items = Object.keys(obj).map(function(key) {
      return [key, obj[key]];
    });
    items.sort(function(first, second) {
      return second[1] - first[1];
    });
    sorted_obj = {}
    $.each(items, function(k, v) {
      use_key = v[0]
      use_value = v[1]
      sorted_obj[use_key] = use_value
    })
    return (sorted_obj)
  }

  // sorted country values
  stop = sortJsObject(top);
  sustop = sortJsObject(ustop);
  sdetop = sortJsObject(detop);
  var places = {};

  function calPlace(name) {
    var place = {}
    place[name] = {
      't': ['t'],
      'c': ['Infizierte gesamt'],
      's': ['s'],
      'r': ['Genesen'],
      'd': ['Verstorben'],
      'a': ['Aktuell Infektiöse']
    }
    // cf = 0, sp = 0, rc = 0, dd = 0, active = 0;
    datasets[0].forEach(function(d) {
      var USTd = new Date(d["datetime"]);

      place[name].t.push(USTd.setHours(USTd.getHours() + 8));

      cf = 0, sp = 0, rc = 0, dd = 0, active = 0;
      current = d;
      delete current["datetime"];

      if (name == "Weltweit") {

        d = current[name];
        if (d == undefined) {
          d = "0"
        };
        items = d.split("-");
        switch (items.length) {
          case 4:
            dd += +items[3];
          case 3:
            rc += +items[2];
          case 2:
            sp += +items[1];
          case 1:
            cf += +items[0];
            break;
        };
        active = cf - dd - rc;



      } else if (name == "china") {




      } else {


        d = current[name];
        if (d == undefined) {
          d = "0"
        };
        items = d.split("-");

        switch (items.length) {
            case 4:
              if (items[3] != 'nan') dd += +items[3];
            case 3:
              if (items[2] != 'nan') rc += +items[2];
            case 2:
              if (items[1] != 'nan') sp += +items[1];
            case 1:
              if (items[0] != 'nan') cf += +items[0];
              break;
        };
        active = cf - dd - rc;

      }
      active = cf - dd - rc;
      place[name].c.push(cf);
      place[name].r.push(rc);
      place[name].d.push(dd);
      place[name].a.push(active);

    });

    return place[name];
  }

  function showPlace(name) {
    if (name in datasets[13]) {
        name = datasets[13][name].de_name
    }

  // Set place name
    $("#placename").text(name.toUpperCase());

  // Set website source
    $("#source-website").html('.....');

  }


  function setFill(enname) {
    var pop = datasets[12][datasets[12].length - 1][enname];
    if (pop == "" || pop == undefined || pop.toString().split("-")[0] == "0") {
      return 'url(img/texture-s.png)'; //non-case country, 0 aggregate confirm
    } else {
      pop = +pop.toString().split("-")[0] - +pop.toString().split("-")[2] - +pop.toString().split("-")[3]; // remaining confirmed
      // return "#00000000";
    }
    if (pop == 0) {
      return 'url(img/texture-sg.png)'; // 0 active confirm
    } else {
      return 'url()';
    }


  }

  function setColor(enname) {
    var id = 0;
    if (enname in datasets[12][0]) {
      var pop = datasets[12][0][enname];
    }
    else {
      var pop = undefined;
    }

    if (pop == undefined) {
      pop = 0;
    }

    if (pop >= 1000) {
      id = 5;
    } else if (pop > 100 && pop <= 1000) {
      id = 4;
    } else if (pop > 10 && pop <= 100) {
      id = 3;
    } else if (pop > 1 && pop <= 10) {
      id = 2;
    } else if (pop > 0.1 && pop <= 1) {
      id = 1;
    } else if (pop > 0 && pop <= 0.1) {
      id = 0;
    } else {
      id = 5;
      return "#00000000";
    }

    return colors[id];
  }


  function style(feature) {
    if (feature.properties.enname == "us" || feature.properties.enname == "canada") {
      return {
        fillOpacity: 0,
        opacity: 0,
      };
    } else { // if(feature.properties.enname != null)
      return {
        fill: setFill(feature.properties.enname),
        fillColor: setColor(feature.properties.enname),
        fillOpacity: 0.4,
        weight: 0.5,
        opacity: 1,
        color: '#b4b4b4',
        dashArray: '2'
      };
    }
  }

  function highlightFeature(e) {
    // e indicates the current event
    var layer = e.target; //the target capture the object which the event associates with
    layer.setStyle({
      weight: 2,
      opacity: 0.8,
      color: '#e3e3e3',
      fillColor: '#00ffd9',
      fillOpacity: 0.1
    });
    // bring the layer to the front.
    layer.bringToFront();
    if (e.target.feature.properties.enname == "us" || e.target.feature.properties.enname == "canada") {
      layer.bringToBack();
    }
  }

  // 3.2.2 zoom to the highlighted feature when the mouse is clicking onto it.
  function zoomToFeature(e) {
    // mymap.fitBounds(e.target.getBounds());
    L.DomEvent.stopPropagation(e);
    $("#hint").text("Click here to the global trend.");

    displayPlace(e.target.feature.properties.enname)
  }

  // 3.2.3 reset the hightlighted feature when the mouse is out of its region.
  function resetHighlight(e) {
    areas.resetStyle(e.target);
    germany.resetStyle(e.target);
  }

  // 3.3 add these event the layer obejct.
  function onEachFeature(feature, layer) {
    layer.on({
      mouseover: highlightFeature,
      click: zoomToFeature,
      mouseout: resetHighlight
    });
  }

  var areas = new L.TopoJSON(datasets[1], {
    style: style,
    onEachFeature: onEachFeature
  }).addTo(mymap);


  var germany = new L.TopoJSON(datasets[10], {
    style: style,
    onEachFeature: onEachFeature
  }).addTo(mymap);


  new L.control.layers(baseLayers, {
    "Fallzahlen": areas,
  }, {
    collapsed: false
  }).addTo(mymap);


$("#hint").on("click", function() {
    displayPlace("Weltweit")
});

mymap.on('click', onMapClick);


function onMapClick(e) {
    $("#hint").click();
}

displayPlace("Weltweit");


  // Global trend line chart
  historyChart = c3.generate({
    size: {
      height: 350,
    },
    data: {
      x: "t",
      y: "confirmed",
      columns: [places["Weltweit"].t, places["Weltweit"].c, places["Weltweit"].a, places["Weltweit"].r,
      places["Weltweit"].d],
      type: 'line',
      axes: {
        confirmed: 'y'
      },
      colors: {
        'Infizierte gesamt': 'SlateBlue',
        // Suspected: 'orange',
        'Aktuell Infektiöse': 'orange',
        'Genesen': '#28a745',
        'Verstorben':  '#dc3545'
      }
    },
    zoom: {
      enabled: true
    },
    axis: {
      x: {
        type: "timeseries",
        tick: {
          format: "%b %d",
          centered: true,
          fit: true,
          count: 8
        }
      },
      y: {
        label: {
          text: 'Cases',
          position: 'outer-middle'
        },
        min: 0,
        padding: {
          bottom: 0
        },
        tick: {
          format: d3locale.format(",.2r")
        },
        type: 'linear'
      }
    },
    point: {
      r: 3,
      focus: {
        expand: {
          r: 5
        }
      }
    },
    zoom: {
      // rescale: true,
      enabled: false,
      type: "scroll",
    },
    tooltip: {
      linked: true,
    },
    legend: {
      position: 'inset',
      inset: {
        anchor: "top-left",
        y: 10
      },
    },
    bindto: "#total-chart"
  });


  predictionChart = c3.generate({
    size: {
      height: 350
    },
    data: {
      x: "t",
      columns: [],
      colors: {
        'Infizierte gesamt': 'SlateBlue' ,
        'Aktuell Infektiöse':'orange',
        'Genesen': '#28a745',
        'Verstorben':'#dc3545',
      },

    },
    point: {
        show: false
      },
      axis: {
    x: {
        type: 'timeseries',
        tick: {
          fit: true,
            format: '%b %d',
            count: 8,

        }
    },
      y: {
        label: {
          text: 'Cases',
          position: 'outer-middle'
        },
        min: 0,
        padding: {
          bottom: 0
        },
        tick: {
          format: d3locale.format(",.2r")
        },
        type: 'linear'
      }
},
    tooltip: {
      linked: true,
    },
    point: {
      r: 1,
      focus: {
        expand: {
          r: 3
        }
      }
    },
    legend: {
      position: 'inset',
      inset: {
        anchor: "top-left",
        y: 10
      },
    },

    bindto: "#prediction-chart"
  });

function displayPlace(name) {
    places[name] = calPlace(name);
    showPlace(name);

    fetch(`/paneldata/${name}`) // Call the fetch function passing the url of the API as a parameter
    .then((resp) => resp.json()) // Transform the data into json
    .then(function(data) {
        len = places[name].t.length;
        $(".confirmed-count").text(formatPanelNumber(data.dc));
        $(".active-count").text(formatPanelNumber(data.di));
        $(".recovered-count").text(formatPanelNumber(data.dr));
        $(".death-count").text(formatPanelNumber(data.dd));

        nc = data.dc_diff
        ni = data.di_diff
        nr = data.dr_diff
        nd = data.dd_diff

        $(".confirmed-new-count").text(formatPanelDiffNumber(data.dc, nc));
        $(".active-new-count").text(formatPanelDiffNumber(data.di, ni));
        $(".recovered-new-count").text(formatPanelDiffNumber(data.dr, nr));
        $(".death-new-count").text(formatPanelDiffNumber(data.dd, nd));

        formatPanelDiffIcon(data.di, ni)
      })
        .catch(function() {
          console.log('Download of panel data failed')
    })

    fetch(`/history/${name}`) // Call the fetch function passing the url of the API as a parameter
    .then((resp) => resp.json()) // Transform the data into json
    .then(function(data) {
        historyChart.load({columns:data.data})
      })
        .catch(function() {
          console.log('Download of history data failed')
    })

    fetch(`/predictions/${name}`) // Call the fetch function passing the url of the API as a parameter
    .then((resp) => resp.json()) // Transform the data into json
    .then(function(data) {
        predictionChart.load({columns:data.data})
      })
        .catch(function() {
          console.log('Download of prediction data failed')
    })
}

function formatPanelNumber(totalNumber) {
    if (totalNumber == 'nan') {
        return 'keine Daten'
    }
    else {
        return totalNumber.toLocaleString()
    }
}

function formatPanelDiffNumber(totalNumber, diffNumber) {
   if (diffNumber > 0) {
       return "+" + diffNumber.toLocaleString();
   }
   else if (diffNumber == 0 || totalNumber == 'nan') {
       return ""
   }
   else {
       return diffNumber.toLocaleString();
   }
}

function formatPanelDiffIcon(di, ni) {
    if (di != 'nan' && (ni > 0 || ni < 0)) {
          document.getElementById("counticon").style.display = "initial";
        }
    else {
          document.getElementById("counticon").style.display = "none";
    }
}

  function makeTable() {

    var topinuse = {};
    if (document.getElementById('areaSwitcher').checked != false) {
      topinuse = sdetop;
    } else {
      topinuse = stop;
    }

    Object.keys(topinuse).slice(0, 51).forEach(function(d) {
      if (d.toUpperCase() == "DIAMOND PRINCESS") {
        return;
      }
      var dlabel = d.toUpperCase();
      if (d.toUpperCase() == "GEORGIA USA") {
        dlabel = "GEORGIA";
      }
      $("tbody").append("<tr id='" + d.replace(" ", "") + "-row'><th style='text-align:right;cursor: pointer;'>" + dlabel + "</th></tr>");
      places[d] = calPlace(d);


      $("tr#" + d.replace(" ", "") + "-row").append('<th style="text-align:right"><i class="cell-' + d.replace(" ", "") + '-aggre-confirmed">' + places[d].c[len - 1].toLocaleString() + '</i></th>');


      $("tr#" + d.replace(" ", "") + "-row").append('<th ><i class="cell-' + d.replace(" ", "") + '">' + "" + '</i></th>');


      var a = [];
      for (var i = len - showLen; i < len; i++) {
        a.push(places[d].a[i]);
      }

      $("." + 'cell-' + d.replace(" ", "")).sparkline(a, {
        type: 'line',
        lineColor: '#bf0000',
        fillColor: '#ffd4aa',
        disableTooltips: true
      });



      for (var i = len - showLen; i < len; i++) {
        // a.push(places[d].a[i]);
        //$("tr#" + d.replace(" ", "") + "-row").append('<th scope="col"><i class="cell">' + places[d].c[i] + '</i></th>');
        $("tr#" + d.replace(" ", "") + "-row").append('<th ><i class="cell cell-' + d.replace(" ", "") + '-' + i.toString() + '">' + "" + '</i></th>');
        bgcolor = "#28a745";
        bdcolor = "#28a745";
        size = "32px";
        bdsize = "0px";
        top = "0rem";
        left = "0rem";


        //all the cases are recovered
        // c ===> 0, 10, 100, 1000, 10000
        // size =>0,  4,   8,   16,    32

        if (places[d].c[i] >= 10000) {
          size = "32px"
          top = "-.45rem";
          left = "-.35rem";


        } else if (places[d].c[i] < 10000 && places[d].c[i] >= 1000) {
          size = "24px";
          top = "-.2rem";
          left = "-.1rem";

        } else if (places[d].c[i] < 1000 && places[d].c[i] >= 100) {
          size = "16px";
          top = "0rem";
          left = ".1rem";;


        } else if (places[d].c[i] < 100 && places[d].c[i] >= 10) {
          size = "10px";
          top = ".25rem";
          left = ".25rem";

        } else if (places[d].c[i] < 10 && places[d].c[i] >= 1) {
          size = "8px";
          top = ".25rem";
          left = ".25rem";
        } else {
          size = "0px";
          bdsize = "0px";
        }


        // a/c rate ==> red, oregen, yellow, green{plus yellow}
        // color ==>  recover:  #28a745
        // a ===>
        acrate = places[d].a[i] / places[d].c[i]

        colors = chroma.scale('YlOrRd').mode('lch').colors(6);
        if (acrate > 0.995) {
          bgcolor = colors[5];
        } else if (acrate >= 0.9 && acrate <= 0.995) {
          bgcolor = colors[4];
        } else if (acrate >= 0.5 && acrate <= 0.9) {
          bgcolor = colors[3];
        } else if (acrate >= 0.3 && acrate <= 0.5) {
          bgcolor = colors[2];
        } else if (acrate >= 0.2 && acrate <= 0.3) {
          bgcolor = colors[1];
        } else if (acrate < 0.2 && places[d].c[i] > 100) {
          bgcolor = '#28a745';
        } else {
          bgcolor = colors[1];

        }

        $('.cell-' + d.replace(" ", "") + '-' + i.toString())
          .css("background-color", bgcolor)
          .css("border", bdsize + " solid " + bdcolor)
          .css("width", size)
          .css("height", size)
          .css("margin-top", top)
          .css("margin-left", left);


      }




      $("tr#" + d.replace(" ", "") + "-row").on("click", function() {
        // places[d] = calPlace(d);
        showPlace(d);

        historyChart.load({
          columns: [places[d].c, places[d].a, places[d].r, places[d].d],
          unload: ['Aggr. Confirmed', 'Active Confirmed', 'Recovered', 'Death'],
        });
        predictionChart.load({
          columns: [places[d].c, places[d].a, places[d].r, places[d].d],
          unload: ['Aggr. Confirmed', 'Active Confirmed', 'Recovered', 'Death'],
        });
      })



    })

  }


  $('#areaSwitcher').change(function() {

    $('.loader').show();

    $("#rankingToggle").css('visibility', 'visible');
    $("#table").css("visibility", "visible");
    $(".leaflet-control-container").css("visibility", "hidden");


    len = places["Weltweit"].t.length
    showLen = 25
    $("thead").html('<th scope="col" style="text-align:right; width:120px !important"><b>Bundesland</b></th>');
    $("thead").append("<th style='text-align:center'>Infizierte gesamt</th>");
    $("thead").append("<th style='text-align:center'>Aktuell Infektiöse Trend</th>");


    Object.values(places["Weltweit"].t.slice(len - 1 - showLen, len - 1)).forEach(function(d) {
      //label = new Date(new Date(d).setHours(new Date(d).getHours() + 8)).toString().substring(4,10);
      label = (new Date(d)).toString().substring(4, 10);
      // console.log(label);
      $("thead").append("<th style='text-align:center' >" + label + "</th>");
    });


    $("tbody").html('');
    makeTable();


    $('.loader').fadeOut("slow");

  })

  $("button").click(function() {
    var b = $(this);
    if(b.hasClass("scale")){
      historyChart.axis.types({y: b.val()});
      predictionChart.axis.types({y: b.val()});
    }
    // if(b.hasClass("relativity")) {return} add values for absolute/relative
  })

  $('#panelSwitcher').change(function() {
    $('.loader').show();

    if (document.getElementById('panelSwitcher').checked == false) {
      $("#table").css("visibility", "hidden");

      $("#rankingToggle").css("visibility", "hidden");
      $(".leaflet-control-container").css("visibility", "visible");
      $("#content-explain").text(
        "In the main map, an country or region's inflection level is measured by the # of today's active/remaining confirmed cases. The gray slash texture indicates no previous cases are discovered; the greentexture means the region in which all the infected cases have recovered."
      );
      $("thead").html('');
      $("tbody").html('');


    } else {

      $("#rankingToggle").css('visibility', 'visible');
      $("#content-explain").text(
        "Diese Übersichtstabelle listet alle Länder oder deutschen Bundesländer in Reihenfolge der aktuell infektiösen \
        Fälle auf. \
        Die Größe und Farbe der einzelnen Quadrate stellt die Zahl und Genesungsrate der Covid-19-Fälle der \
        letzten 20 Tage in den einzelnen Regionen dar. \
        Dabei gibt die Größe der Blöcke die Gesamtzahl infizierter Fälle an. \
        Die Farbe der Blöcke wechselt von rot über orange und gelb nach grün, während der Anteil der Genesenen zunimmt."
      );
      $("#table").css("visibility", "visible");
      $(".leaflet-control-container").css("visibility", "hidden");

      $("#rankingText").remove();
      $("#rankingIcon").remove();
      $("#rankingSpace").remove();



      len = places["Weltweit"].t.length
      showLen = 20
      $("thead").html('<th scope="col" style="text-align:right; width:120px !important"><b>Land</b></th>');

      $("thead").append("<th style='text-align:center'>Infizierte gesamt </th>");
      $("thead").append("<th style='text-align:center'>Aktuell Infektiöse Trend</th>");

      Object.values(places["Weltweit"].t.slice(len - 1 - showLen, len - 1)).forEach(function(d) {
        label = (new Date(d)).toString().substring(4, 10);
        $("thead").append("<th style='text-align:center' >" + label + "</th>");
      });


      $("tbody").html('');


      makeTable();


    }
    $('.loader').fadeOut("slow");

  })

  $(".leaflet-control-attribution")
    .css("background-color", "transparent")
    .html("");



});